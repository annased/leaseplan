# LeasePlan

## Javascript, Webdriver IO and Cucumber/Gherkin Setup Guide
This is a Test Automation framework designed using JS, Webdriver IO and Cucumber

## Framework Structure
```
├───features          # Single directory at the root of the project where all of cucumber features will belong to, here are additional and support directories
    ──common          # This folder contains all the common pages, constants, data , files needed for whole project
    ──pages           # This folder contains projects parts as page classes
    ──steps           # This folder contains step definitions for every page
    ──tests           # This folder contains test files - features to run
├───reports           # This folder contains test result (includes html report and screenshots)
└───wdio.conf.js      # Webdriver IO config file

```
## To Get Started

### Pre-requisites
* Download and install Chrome browser latest version
* Download and install Node.js
* Optional - Download and install any Text Editor like Visual Code/Webstorm
  * Install the necessary extensions (e.g. VSCode Great Icons, Cucumber (Gherkin) Full Support, Snippets and Syntax Highlight for Gherkin (Cucumber))

### Setup Env Step by Step
* Clone the repository into a folder
* Go to Project root directory and install Dependancy: `npm install` 
All the dependencies from package.json would be installed in node_modules folder


### How to write Test
* Create folders under pages, steps and tests folders as <test-suite-name>
* Add new selectors and methods under <pages> folder
* Name the file as <pagename.page>.js
* Add steps methods under <steps> folder
* Name the file as <pagename.steps>.js
* Add new spec files under <tests> folder
* Name the file as <testname>.feature

### How to Run Test
* Run complete All Tests: npm all
* Run complete Test Spec: npm run spec features/tests/<test-suite-name>/<pagename>/<testname>/<testname>.feature
