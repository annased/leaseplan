const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class CookiePage extends Page {
    /**
     * define selectors using getter methods
     */
    get btnAllowCookie () { return $('.optanon-allow-all.accept-cookies-button') }

    async clickOnAllowCookie () {
        await (await this.btnAllowCookie).click();
    }
}

module.exports = new CookiePage();
