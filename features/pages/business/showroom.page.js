const Page = require('../../common/page');
const Urls = require('../../common/consts/urls.constant')

/**
 * page containing selectors and methods for showroom page
 */
class ShowRoomPage extends Page {
    /**
     * define selectors from Showroom Page
     */
    get popularFilters () { return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(1) > div > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3') }
    get optionBestDeals () { return $('div:nth-child(1) > div > div > label > span > span.sc-gsTCUz.bKzEGB') }
    get optionConfigureYourself () { return $('#Configure\ yourself') }
    get optionFastDelivery () { return $('#Fast\ delivery') }
    get makeModel () { return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2) > div.sc-bqyKva.RoundedPaper-sc-1a43p54.ButtonWrapper-sc-3tbx2n.iRYRjy.bZTdUO.eMxuRR > div > button > div > div > div.sc-cOajty.MobileGrowGridItem-sc-1ik14oq.SGdCX.iUpVCR > h3') }
    get btnSave () { return $('button.sc-bYEvPH.WidthContainedButton-sc-1an5ad7.gtczwt.cVMtBp') }
    get resetFilters () { return $('[data-key=\"resetFilters\"]') }
    get searchInputMakeModel () { return $('.sc-dIUggk.jyuJgK') }
    get optionAudi () { return $('#make-AUDI') }
    get carListTitles () { return $$('h2.sc-giIncl.fbETia')}
    get lblPopularmakes () { return $('#app > div > div > div > div > div > div.sc-jrAGrp.Spacing-sc-1vlvqzt.lfGwzl.jSqLYO > div.sc-jrAGrp.eQgiFV > div.sc-hlTvYk.jKGkpW > div > div > div > div > div > div:nth-child(1) > div:nth-child(2) > div.sc-hKgILt.RoundedPaper-sc-1a43p54.OnPageFilterPaper-sc-uw3g20.chrFRZ.bZTdUO.fa-Dlwl > div > div.sc-bdfBwQ.bUgaxv > div > div > div:nth-child(1) > div.Badge-sc-18o5ns2.fdWEvw > span') }

    /**
     * a method to select filter and option of the filter
     * @param filter which should be selected
     * @param option from selected filter
     */
    async selectOption (filterName, optionName) {
         await (await this[filterName]).click();
         await (await this[optionName]).click({x:30});
         await (await this.btnSave).click();
    }

    /**
     * a method to reset all the filters
     */
    async clickOnresetFilters () {
        await (await this.resetFilters).click();
    }
    
    /**
     * a method to select filter 
     * @param filter which should be selected
     */
    async selectFilter (filterName) {
        await (await this[filterName]).click();
    }
    /**
     * a method to fill in search input of Make Model filter
     */
    async fillSearchInputMakeModel (filterName, value) {
         await (await this.selectFilter(filterName));
         await (await this.searchInputMakeModel).click();
         await (await this.searchInputMakeModel).setValue(value);
    }
    
    /**
     * open specific page from LeasePlan website by giving the path
     */
    open () {
        return super.open(Urls.PATHS.SHOWROOM);
    }
}

module.exports = new ShowRoomPage();