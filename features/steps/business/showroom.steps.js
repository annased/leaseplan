const { Given, When, Then } = require('@cucumber/cucumber');

const ShowRoomPage = require('../../pages/business/showroom.page');
const CookiePage = require('../../common/cookie.page');

const pages = {
    showroom: ShowRoomPage,
    cookie: CookiePage
}

Given(/^I am on the (\w+) page$/, async (page) => {
    await pages[page].open()
});

Given(/^I allow cookies on website/, async () => {
    await pages.cookie.clickOnAllowCookie();
});

When(/^I select (\w+) and (.*)$/, async (filterName, optionName) => {
    await pages.showroom.selectOption(filterName, optionName)
});

When(/^The (\w+) filter is selected and I type (.*)$/, async (filterName, value) => {
    await pages.showroom.fillSearchInputMakeModel(filterName, value)
});    

Then(/^I should see filter name is (.*)$/, async (changedName) => {
    await expect(pages.showroom.popularFilters).toHaveTextContaining(changedName);
});

Then(/^The filter is disappearing/, async () => {
    await expect(pages.showroom.lblPopularmakes).not.toExist();
});

When(/^I reset filters/, async () => {
    await pages.showroom.clickOnresetFilters()
});

Then(/^All the car titles are containing (.*)$/, async (text) => {
    (await pages.showroom.carListTitles).forEach(element => {
        expect(element.getText()).toHaveTextContaining(text)})
});

