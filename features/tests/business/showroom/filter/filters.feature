Feature: Filtering functionality in Showroom 

Background: 
Given I am on the showroom page

  Scenario: As a user, I should use filter for quick search

    When  I allow cookies on website
    When  I select <filter> and <option>
    Then  I should see filter name is <changedName>

    Examples:
      | filter         | option         | changedName |
      | popularFilters | optionBestDeals| Best deals  |
     
  Scenario: As a user, I want to reset all filters
 
    When I reset filters
    Then I should see filter name is <filter>

    Examples:
      | filter          | 
      | Popular filters | 

  
  Scenario: As a user, I select invalid data

    When The <filter> filter is selected and I type <value>
    Then The filter is disappearing

    Examples:
      | filter         | value       |
      | makeModel      | test        |    

  Scenario: As a user, I have proper car list after filtering
    When  I select <filter> and <option>
    Then All the car titles are containing <text>
    
    Examples: 
    | filter         | option     | text |
    | makeModel      | optionAudi | Audi | 